import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppSubmitComponent } from './app-submit.component';

describe('AppSubmitComponent', () => {
  let component: AppSubmitComponent;
  let fixture: ComponentFixture<AppSubmitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppSubmitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppSubmitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
