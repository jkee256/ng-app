import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-submit',
  templateUrl: './app-submit.component.html',
  styleUrls: ['./app-submit.component.sass']
})
export class AppSubmitComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }

}
