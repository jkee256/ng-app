import { Component, OnInit } from '@angular/core';
import { AppSubmitComponent } from '../app-submit/app-submit.component'
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-content',
  templateUrl: './app-content.component.html',
  styleUrls: ['./app-content.component.sass']
})
export class AppContentComponent implements OnInit {
  inputs = {
    'name': {
      'value': '',
      isWrong: false,
      'validate': () => {
        if (this.inputs.name.value.trim() == '')
          this.inputs.name.isWrong = true
        else
          this.inputs.name.isWrong = false
        return this.inputs.name.isWrong
      }
    },
    'password': {
      'value': '',
      isWrong: false,
      'validate': () => {
        if (!this.inputs.password.value ||
            this.inputs.password.value.length < 8)
          this.inputs.password.isWrong = true
        else
          this.inputs.password.isWrong = false
        return this.inputs.password.isWrong
      }
    }
  }
  log = () => {
    let errors = []
    if (this.inputs.name.validate())
      errors.push('name')
    if (this.inputs.password.validate())
      errors.push('password')
    if (errors.length == 0)
      this.openDialog()
    /*
    alert(
      'Username: '+this.inputs['name'].value+
      (this.inputs.name.isWrong?' Empty':'')+'\n'+
      'Password: '+this.inputs['password'].value+
      (this.inputs.password.isWrong?' Empty or length < 8':'')
    )
    */
  }
  forgot = () => {
    alert('forgot?')
  }

  openDialog() {
    this.dialog.open(AppSubmitComponent, {
      data: {
        name: this.inputs.name.value
      }
    });
  }

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

}
