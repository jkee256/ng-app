import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'pretty-input',
  templateUrl: './pretty-input.component.html',
  styleUrls: ['./pretty-input.component.sass']
})
export class PrettyInputComponent implements OnInit {
  @Input() titleS: String
  @Input() type: String
  @Input() model

  focusout = () => {
    this.model.validate()
  }

  constructor() { }

  ngOnInit() {
  }

}
