import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppLogoComponent } from './app-logo/app-logo.component';
import { AppContentComponent } from './app-content/app-content.component';
import { AppFooterComponent } from './app-footer/app-footer.component';
import { PrettyInputComponent } from './pretty-input/pretty-input.component';
import { AppSubmitComponent } from './app-submit/app-submit.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material'

@NgModule({
  declarations: [
    AppComponent,
    AppLogoComponent,
    AppContentComponent,
    AppFooterComponent,
    PrettyInputComponent,
    AppSubmitComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MatDialogModule
  ],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ],
  bootstrap: [AppComponent],
  entryComponents: [AppSubmitComponent]
})
export class AppModule { }
